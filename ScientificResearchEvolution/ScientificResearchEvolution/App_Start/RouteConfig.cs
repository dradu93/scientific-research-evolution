﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ScientificResearchEvolution
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ACM Plot",
                url: "AcmPlot/Create",
                defaults: new { controller = "AcmPlot", action = "Create" }
                );

            routes.MapRoute(
                name: "Plot",
                url: "home/myaction",
                defaults: new { controller = "Home", action = "MyAction" }
                );

            routes.MapRoute(
                name: "Update DBLP",
                url: "update/updateDBLP",
                defaults: new { controller = "Update", action = "UpdateDBLP" }
                );

            routes.MapRoute(
                name: "Update ACM",
                url: "update/updateACM",
                defaults: new { controller = "Update", action = "UpdateACM" }
                );


            routes.MapRoute(
                name: "Update",
                url: "update/update",
                defaults: new { Controller = "Update", action = "update" }
                );

            routes.MapRoute(
                name: "ACM plotting",
                url: "acm/view",
                defaults: new { Controller = "AcmPlot", action = "acmplot" }
                );

            routes.MapRoute(
                name: "DBLP plotting",
                url: "dblp/dblp",
                defaults: new { Controller = "Dblp", action = "dblp" }
                );
            /*
            routes.MapRoute(
                name: "Download DBLP",
                url: "home/ChartArrayBasic",
                defaults: new { controller = "Home", action = "Index" }
                );
            */
            

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
