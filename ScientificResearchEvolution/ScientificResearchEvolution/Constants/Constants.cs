﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ScientificResearchEvolution.Constants
{
    public static class Constants
    {
        public static string DatabaseConnectionString =
            @"Data Source=DESKTOP-H6J70TH\SQLEXPRESS;AttachDbFilename=C:\Users\Radu\Documents\ScientificResearchEvolution.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";

        public static class URLs
        {
            public static string DBLP = "http://dblp.uni-trier.de/xml/dblp.xml.gz";
            public static string ACM = "http://dl.acm.org/feeds/dl_contents.csv";
        }

        public static class LocalStoragePaths
        {
            public static string HDD = "E:\\";
            public static string DBLPArchive = HDD + "dblp\\dblp.xml.gz";
            public static string DBLP = HDD + "dblp\\dblp.xml";
            public static string DBLP_DTD = HDD + "dblp\\dblp.dtd";            
            public static string DBLPArticles = HDD + "dblp\\articles.txt";
            public static string DBLPPhdThesis = HDD + "dblp\\phdthesis.txt";
            public static string DBLPInproceedings = HDD + "dblp\\inproceedings.txt";
            public static string DBLPWww = HDD + "dblp\\www.txt";

            public static string ACM = HDD + "acm\\acm.csv";
            public static string ACMTemporaryOutput = HDD + "acm\\acm.txt";
        }

        public static class TagRegex
        {
            public static Regex Title = new Regex("<title>(.*)</title>");
            public static Regex Pages = new Regex("<pages>(.*)</pages>");
            public static Regex Volume = new Regex("<volume>(.*)</volume>");
            public static Regex Year = new Regex("<year>(.*)</year>");
            public static Regex Journal = new Regex("<journal>(.*)</journal>");
            public static Regex Url = new Regex("<url>(.*)</url>");
            public static Regex Ee = new Regex("<ee>(.*)</ee>");
            public static Regex School = new Regex("<school>(.*)</school>");
            public static Regex Note = new Regex("<note>(.*)</note>");
            public static Regex Booktitle = new Regex("<booktitle>(.*)</booktitle>");
            public static Regex Crossref = new Regex("<crossref>(.*)</crossref>");

            public static class ACM
            {
                public static Regex StringValue = new Regex("\"(.*)\"");
                public static Regex PublishingVolumeAndDate = new Regex(@"(.*) \(([^)]*)\)");
                public static Regex PublishingVolumeAndPreciseDate = new Regex("(.*) ");
            }

            
        }

        public static class TableNames
        {
            public static class Dblp
            {
                private static string Prefix = "Dblp";
                public static string Article = Prefix + "Article";
                public static string Inproceedings = Prefix + "Inproceedings";
                public static string PhdThesis = Prefix + "PhdThesis";
                public static string Www = Prefix + "Www";
            }

            public static class Acm
            {
                private static string Prefix = "Acm";
                public static string Journal = Prefix + "Journal";
                public static string Transaction = Prefix + "Transaction";
                public static string Magazine = Prefix + "Magazine";
                public static string Publication = Prefix + "Publication";
                public static string SigNewsletter = Prefix + "SigNewsletter";
                public static string ConferenteProceedings = Prefix + "ConferenceProceedings";
            }
        }

        public static class Categories
        {
            public static class Dblp
            {

            }

            public static class Acm
            {
                public static string Journals = "Journals";
                public static string Transactions = "Transactions";
                public static string Magazines = "Magazines";
                public static string Publications = "Publications";
                public static string SigNewsletters = "SIG Newsletters";
                public static string ConferenceProceedings = "Conference Proceedings";              
            }
        }

    }
}