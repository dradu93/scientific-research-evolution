﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScientificResearchEvolution.ResearchTypes.Acm;
using ScientificResearchEvolution.Models;
using NHibernate.Linq;

namespace ScientificResearchEvolution.Controllers
{
    public class AcmPlotController : Controller
    {
        // GET: AcmPlot
        [ActionName("acmplot")]
        public ActionResult Index()
        {
            return View("View");
        }

        // GET: AcmPlot/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AcmPlot/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AcmPlot/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                //var startMonth = int.Parse(collection["StartMonth"]);
                var startYear = int.Parse(collection["StartYear"]);
                //var endMonth = int.Parse(collection["EndMonth"]);
                var endYear = int.Parse(collection["EndYear"]);
                

                var cp = new List<ConferenceProceeding>();
                var journals = new List<Journal>();
                var magazines = new List<Magazine>();
                var publications = new List<Publication>();
                var signews = new List<SIGNewsletter>();
                var transactions = new List<Transaction>();

                //var x = collection["ConferenceProceedings"];

                var cpChecked = !collection["ConferenceProceedings"].Equals("false");
                var journalsChecked = !collection["Journals"].Equals("false");
                var magazinesChecked = !collection["Magazines"].Equals("false");
                var publicationsChecked = !collection["Publications"].Equals("false");
                var snChecked = !collection["SIGNewsletters"].Equals("false");
                var transactionsChecked = !collection["Transactions"].Equals("false");

                var title = "ACM: ";

                if (cpChecked)
                {
                    
                    title += "Conference Proceedings ";
                }

                if (journalsChecked)
                {
                    
                    if(!title.Equals("ACM: "))
                    {
                        title += "+ ";
                    }
                    title += "Journals ";
                }

                if (magazinesChecked)
                {
                    
                    if (!title.Equals("ACM: "))
                    {
                        title += "+ ";
                    }
                    title += "Magazines ";
                }

                if (publicationsChecked)
                {
                    
                    if (!title.Equals("ACM: "))
                    {
                        title += "+ ";
                    }
                    title += "Publications ";
                }

                if (snChecked)
                {
                    
                    if (!title.Equals("ACM: "))
                    {
                        title += "+ ";
                    }
                    title += "SIG Newsletters ";
                }

                if (transactionsChecked)
                {
                    
                    if (!title.Equals("ACM: "))
                    {
                        title += "+ ";
                    }
                    title += "Transactions ";
                }

                var x = new List<int>();
                var y = new List<int>();
                

                for (var year = startYear; year <= endYear; ++year)
                {
                    x.Add(year);

                    if (cpChecked)
                    {
                        cp = NHibernateHelper.Session.Query<ConferenceProceeding>()
                        .Where(t => t.FirstPublishedDateYear.Equals(year))
                        .ToList();
                    }

                    if (journalsChecked)
                    {
                        journals = NHibernateHelper.Session.Query<Journal>()
                        .Where(t => t.FirstPublishedDateYear.Equals(year))
                        .ToList();
                    }

                    if (magazinesChecked)
                    {
                        magazines = NHibernateHelper.Session.Query<Magazine>()
                                                .Where(t => t.FirstPublishedDateYear.Equals(year))
                                                .ToList();
                    }

                    if (publicationsChecked)
                    {
                        publications = NHibernateHelper.Session.Query<Publication>()
                        .Where(t => t.FirstPublishedDateYear.Equals(year))
                        .ToList();
                    }

                    if (snChecked)
                    {
                        signews = NHibernateHelper.Session.Query<SIGNewsletter>()
                        .Where(t => t.FirstPublishedDateYear.Equals(year))
                        .ToList();
                    }

                    if (transactionsChecked)
                    {
                        transactions = NHibernateHelper.Session.Query<Transaction>()
                        .Where(t => t.FirstPublishedDateYear.Equals(year))
                        .ToList();
                    }
                    

                    var sum = cp.Count + journals.Count + magazines.Count 
                        + publications.Count + signews.Count + transactions.Count;



                    y.Add(sum);
                }


                System.Web.HttpContext.Current.Cache["x"] = x;
                System.Web.HttpContext.Current.Cache["y"] = y;
                System.Web.HttpContext.Current.Cache["title"] = title;

                return View("AcmPlotResult");



                //return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: AcmPlot/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AcmPlot/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AcmPlot/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AcmPlot/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
