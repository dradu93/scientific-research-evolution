﻿using NHibernate.Linq;
using ScientificResearchEvolution.Models;
using ScientificResearchEvolution.ResearchTypes.Dblp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ScientificResearchEvolution.Controllers
{
    public class DblpPlotController : Controller
    {
        // GET: DblpPlot
        [ActionName("dblpplot")]
        public ActionResult Index()
        {
            return View("Create");
        }

        // GET: DblpPlot/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DblpPlot/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DblpPlot/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var startyear = int.Parse(collection["StartYear"]);
                var endyear = int.Parse(collection["EndYear"]);
                var articles = new List<Article>();
                var phdthesis = new List<PhdThesis>();

                articles = NHibernateHelper.Session.Query<Article>()
                    .Where(t => t.Year >= startyear
                    && t.Year <= endyear)
                    .ToList();

                phdthesis = NHibernateHelper.Session.Query<PhdThesis>()
                    .Where(t => t.Year >= startyear
                    && t.Year <= endyear)
                    .ToList();

                var x = new List<int>();
                var y = new List<int>();

                var title = "DBLP: ";
                var articlesChecked = !collection["Articles"].Equals("false");
                var phdthesisChecked = !collection["PhdThesis"].Equals("false");
                if (articlesChecked)
                {
                    title += "articles ";
                }
                if (phdthesisChecked)
                {
                    title += "+ phdthesis";
                }

                for(var year = startyear; year <= endyear; ++year)
                {
                    x.Add(year);
                    List<Article> a = new List<Article>();
                    List<PhdThesis> p = new List<PhdThesis>(); ;

                    if (articlesChecked)
                    {
                        a = articles.Where(t => t.Year.Equals(year)).ToList();
                    }
                    if (phdthesisChecked)
                    {
                        p = phdthesis.Where(t => t.Year.Equals(year)).ToList();
                    }
                    

                    y.Add(a.Count() + p.Count());
                }

                System.Web.HttpContext.Current.Cache["x"] = x;
                System.Web.HttpContext.Current.Cache["y"] = y;
                System.Web.HttpContext.Current.Cache["title"] = title;

                return View("DblpPlotResult");
                //return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DblpPlot/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DblpPlot/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DblpPlot/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DblpPlot/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
