﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using ScientificResearchEvolution.Models;
using ScientificResearchEvolution.Enums;

namespace ScientificResearchEvolution.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        public ActionResult MyAction(string x)
        {
            //do whatever
            //return sth :)
            return null;
        }

    }
}