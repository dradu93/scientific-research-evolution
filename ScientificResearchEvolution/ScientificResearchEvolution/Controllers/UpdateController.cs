﻿using ScientificResearchEvolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ScientificResearchEvolution.Controllers
{
    public class UpdateController : Controller
    {
        // GET: Update
        [ActionName("update")]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Update the DBLP library
        /// </summary>
        /// <returns></returns>
        [ActionName("UpdateDBLP")]
        public ActionResult UpdateDBLP()
        {
            try
            {
                // download the archive
                using (var client = new WebClient())
                {
                    DataProcessingServices.ProcessOnlineLibrary(Enums.Library.DBLP, client);
                }
            }
            catch (Exception ex)
            {

            }
            return View("Update");
        }

        /// <summary>
        /// Update the ACM library
        /// </summary>
        /// <returns></returns>
        [ActionName("UpdateACM")]
        public ActionResult UpdateACM()
        {
            try
            {   // download the archive
                using (var client = new WebClient())
                {
                    DataProcessingServices.ProcessOnlineLibrary(Enums.Library.ACM, client);
                }
            }
            catch (Exception ex)
            {

            }
            return View("Update");
        }
    }
}