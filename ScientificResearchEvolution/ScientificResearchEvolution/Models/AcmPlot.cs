﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.Models
{
    public class AcmPlot : Plot
    {
        [DisplayName("Conference proceedings")]
        public bool ConferenceProceedings { get; set; }

        [DisplayName("Journals")]
        public bool Journals { get; set; }

        [DisplayName("Magazines")]
        public bool Magazines { get; set; }

        [DisplayName("Publications")]
        public bool Publications { get; set; }

        [DisplayName("SIG Newsletters")]
        public bool SIGNewsletters { get; set; }

        [DisplayName("Transactions")]
        public bool Transactions { get; set; }

        //[DisplayName("Start month")]
        //public int StartMonth { get; set; }

        [DisplayName("Start year")]
        public int StartYear { get; set; }

        //[DisplayName("End month")]
        //public int EndMonth { get; set; }

        [DisplayName("End year")]
        public int EndYear { get; set; }
    }
}