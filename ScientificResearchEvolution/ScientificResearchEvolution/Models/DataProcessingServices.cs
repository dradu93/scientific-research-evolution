﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using ScientificResearchEvolution.Enums;
using ScientificResearchEvolution.Constants;
using System.Xml;
using System.Xml.Linq;
using ScientificResearchEvolution.ResearchTypes;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using ScientificResearchEvolution.ResearchTypes.Acm;

namespace ScientificResearchEvolution.Models
{
    public static class DataProcessingServices
    {

        private static void DownloadLibrary(Library library, WebClient client)
        {
            string webPath, localPath;
            Task downloadTask;
            switch (library)
            {
                case Library.DBLP:
                    webPath = Constants.Constants.URLs.DBLP;
                    localPath = Constants.Constants.LocalStoragePaths.DBLPArchive;
                    break;
                case Library.ACM:
                    webPath = Constants.Constants.URLs.ACM;
                    localPath = Constants.Constants.LocalStoragePaths.ACM;
                    break;
                default:
                    return;
            }
            downloadTask = Task.Factory.StartNew(
                        () => client.DownloadFile(webPath, localPath)
                    );
            Task.WaitAny(downloadTask);
        }
        

        public static void UnzipDBLP()
        {
            // unzip the document
            using (var outputXmlFile = new FileStream(Constants.Constants.LocalStoragePaths.DBLP, FileMode.Create))
            using (var decompressedFileStream = new GZipStream(File.OpenRead(Constants.Constants.LocalStoragePaths.DBLPArchive)
                , CompressionMode.Decompress))
            {
                var decompressTask = Task.Factory.StartNew(
                    () => decompressedFileStream.CopyTo(outputXmlFile)
                );
                Task.WaitAny(decompressTask);
            }
        }

        public static void ProcessDBLPArticle(StreamReader stream, StreamWriter output)
        {
            //var article = new Article();
            string line;
            string title = "", pages = "", volume = "", year = "", journal = "", url = "", ee = "";
            while (!(line = stream.ReadLine()).StartsWith("</article>"))
            {
                if (line.StartsWith("<author>"))
                {
                    continue;
                }
                else if (line.StartsWith("<title>"))
                {
                    var v = Constants.Constants.TagRegex.Title.Match(line);
                    title = v.Groups[1].Value;
                }
                else if (line.StartsWith("<pages>"))
                {
                    var v = Constants.Constants.TagRegex.Pages.Match(line);
                    pages = v.Groups[1].Value;
                }
                else if (line.StartsWith("<year>"))
                {
                    var v = Constants.Constants.TagRegex.Year.Match(line);
                    year = v.Groups[1].Value;
                }
                else if (line.StartsWith("<volume>"))
                {
                    var v = Constants.Constants.TagRegex.Volume.Match(line);
                    volume = v.Groups[1].Value;
                }
                else if (line.StartsWith("<journal>"))
                {
                    var v = Constants.Constants.TagRegex.Journal.Match(line);
                    journal = v.Groups[1].Value;
                }
                else if (line.StartsWith("<url>"))
                {
                    var v = Constants.Constants.TagRegex.Url.Match(line);
                    url = v.Groups[1].Value;
                }
                else if (line.StartsWith("<ee>"))
                {
                    var v = Constants.Constants.TagRegex.Ee.Match(line);
                    ee = v.Groups[1].Value;
                }
                else
                {
                    continue;
                }
            }
            var guid = Guid.NewGuid().ToString();
            output.WriteLine(guid + "|||" + title + "|||" + pages + "|||" 
                + year + "|||" + volume + "|||" + journal + "|||" + url + "|||" + ee);
        }

        public static void ProcessDBLPInproceedings(StreamReader stream, StreamWriter output)
        {
            string line, title = "", pages = "", year = "", booktitle = "", ee = "", crossref = "", url = "";

            while (!(line = stream.ReadLine()).StartsWith("</inproceedings>"))
            {
                if (line.StartsWith("<author>"))
                {
                    continue;
                }
                else if (line.StartsWith("<title>"))
                {
                    var v = Constants.Constants.TagRegex.Title.Match(line);
                    title = v.Groups[1].Value;
                }
                else if (line.StartsWith("<pages>"))
                {
                    var v = Constants.Constants.TagRegex.Pages.Match(line);
                    pages = v.Groups[1].Value;
                }
                else if (line.StartsWith("<year>"))
                {
                    var v = Constants.Constants.TagRegex.Year.Match(line);
                    year = v.Groups[1].Value;
                }
                else if (line.StartsWith("<booktitle>"))
                {
                    var v = Constants.Constants.TagRegex.Booktitle.Match(line);
                    booktitle = v.Groups[1].Value;
                }
                else if (line.StartsWith("<ee>"))
                {
                    var v = Constants.Constants.TagRegex.Ee.Match(line);
                    ee = v.Groups[1].Value;
                }
                else if (line.StartsWith("<crossref>"))
                {
                    var v = Constants.Constants.TagRegex.Crossref.Match(line);
                    crossref = v.Groups[1].Value;
                }
                else if (line.StartsWith("<url>"))
                {
                    var v = Constants.Constants.TagRegex.Url.Match(line);
                    url = v.Groups[1].Value;
                }
            }

            var guid = Guid.NewGuid().ToString();

            output.WriteLine(guid + "|||" + title + "|||" + pages + "|||" + year + "|||" + booktitle + "|||" + ee + "|||" + crossref + "|||" + url);

        }

        public static void ProcessDBLPPhdThesis(StreamReader stream, StreamWriter output)
        {
            string line, title = "", year = "", school = "", ee = "", note = "";

            while (!(line = stream.ReadLine()).StartsWith("</phdthesis>"))
            {
                if (line.StartsWith("<author>"))
                {
                    continue;
                }
                else if (line.StartsWith("<title>"))
                {
                    var v = Constants.Constants.TagRegex.Title.Match(line);
                    title = v.Groups[1].Value;
                }
                else if (line.StartsWith("<year>"))
                {
                    var v = Constants.Constants.TagRegex.Year.Match(line);
                    year = v.Groups[1].Value;
                }
                else if (line.StartsWith("<school>"))
                {
                    var v = Constants.Constants.TagRegex.School.Match(line);
                    school = v.Groups[1].Value;
                }
                else if (line.StartsWith("<ee>"))
                {
                    var v = Constants.Constants.TagRegex.Ee.Match(line);
                    ee = v.Groups[1].Value;
                }
                else if (line.StartsWith("note"))
                {
                    var v = Constants.Constants.TagRegex.Note.Match(line);
                    note = v.Groups[1].Value;
                }
                else
                {
                    continue;
                }
            }
            var guid = Guid.NewGuid().ToString();
            output.WriteLine(guid + "|||" + title + "|||" + year + "|||" + school + "|||" + ee + "|||" + note);
        }

        public static void ProcessDBLPWww(StreamReader stream, StreamWriter output)
        {
            string line, title = "";

            while (!(line = stream.ReadLine()).StartsWith("</phdthesis>"))
            {
                if (line.StartsWith("<author>"))
                {
                    continue;
                }
                else if (line.StartsWith("<title"))
                {
                    var v = Constants.Constants.TagRegex.Title.Match(line);
                    title = v.Groups[1].Value;
                }
            }
            var guid = Guid.NewGuid().ToString();
            output.WriteLine(guid + "|||" + title);

        }


        public static void SafeBulk(SqlCommand bulkCommand)
        {
            try
            {
                bulkCommand.ExecuteNonQuery();
            }
            catch(Exception ex)
            {

            }
        }

        public static void StoreDBLP()
        {

            using (var stream = new StreamReader(Constants.Constants.LocalStoragePaths.DBLP))
            using (var sqlConnection = new SqlConnection(Constants.Constants.DatabaseConnectionString))
            {
                sqlConnection.Open();

                var line = stream.ReadLine();
                line = stream.ReadLine();
                line = stream.ReadLine();

                if (!line.Equals("<dblp>"))
                {
                    throw new Exception("Incorrect file format!");
                }
                var count = 0;

                var articleInsertCommand = new SqlCommand(
                    @"BULK INSERT " + Constants.Constants.TableNames.Dblp.Article + " FROM '" 
                    + Constants.Constants.LocalStoragePaths.DBLPArticles + "' WITH ( FIELDTERMINATOR = '|||', ROWTERMINATOR = '\n' );",
                    sqlConnection
                );
                articleInsertCommand.CommandTimeout = 0;

                var phdThesisInsertCommand = new SqlCommand(
                    @"BULK INSERT " + Constants.Constants.TableNames.Dblp.PhdThesis + " FROM '" 
                    + Constants.Constants.LocalStoragePaths.DBLPPhdThesis + "' WITH ( FIELDTERMINATOR = '|||', ROWTERMINATOR = '\n' );",
                    sqlConnection
                );
                phdThesisInsertCommand.CommandTimeout = 0;

                var inproceedingsInsertCommand = new SqlCommand(
                    @"BULK INSERT " + Constants.Constants.TableNames.Dblp.Inproceedings + " FROM '" 
                    + Constants.Constants.LocalStoragePaths.DBLPInproceedings + "' WITH ( FIELDTERMINATOR = '|||', ROWTERMINATOR = '\n' );",
                    sqlConnection
                );
                inproceedingsInsertCommand.CommandTimeout = 0;

                var wwwInsertCommand = new SqlCommand(
                    @"BULK INSERT " + Constants.Constants.TableNames.Dblp.Www + " FROM '" 
                    + Constants.Constants.LocalStoragePaths.DBLPWww + "' WITH ( FIELDTERMINATOR = '|||', ROWTERMINATOR = '\n' );",
                    sqlConnection
                );
                wwwInsertCommand.CommandTimeout = 0;

                using (var articlesFile = File.CreateText(Constants.Constants.LocalStoragePaths.DBLPArticles))
                using (var phdThesisFile = File.CreateText(Constants.Constants.LocalStoragePaths.DBLPPhdThesis))
                using (var inproceedingsFile = File.CreateText(Constants.Constants.LocalStoragePaths.DBLPInproceedings))
                using (var wwwFile = File.CreateText(Constants.Constants.LocalStoragePaths.DBLPWww))
                {
                    while ((line = stream.ReadLine()) != null)
                    {
                        if (line.StartsWith("<article"))
                        {
                            ProcessDBLPArticle(stream, articlesFile);
                        }
                        else if (line.StartsWith("<phdthesis"))
                        {
                            ProcessDBLPPhdThesis(stream, phdThesisFile);
                        }
                        else if (line.StartsWith("<inproceedings"))
                        {
                            ProcessDBLPInproceedings(stream, inproceedingsFile);
                        }
                        else if (line.StartsWith("<www"))
                        {
                            ProcessDBLPWww(stream, wwwFile);
                        }
                        /*
                        if (++count % 10000 == 0)
                        {
                            //NHibernateHelper.Session.Flush();
                        }
                        */
                    }
                }

                SafeBulk(articleInsertCommand);
                SafeBulk(wwwInsertCommand);
                SafeBulk(phdThesisInsertCommand);
                SafeBulk(inproceedingsInsertCommand);
                
            }
            

        }
             
        /// <summary>
        /// Convert month from name to number
        /// </summary>
        /// <param name="monthName">Friendly name of the month</param>
        /// <returns></returns>
        private static int MonthNameToNumber(string monthName)
        {
            var value = -1;
            switch (monthName)
            {
                case "January":
                    value = 1; break;
                case "February":
                    value = 2; break;
                case "March":
                    value = 3; break;
                case "April":
                    value = 4; break;
                case "May":
                    value = 5; break;
                case "June":
                    value = 6; break;
                case "July":
                    value = 7; break;
                case "August":
                    value = 8; break;
                case "September":
                    value = 9; break;
                case "October":
                    value = 10; break;
                case "November":
                    value = 11; break;
                case "December":
                    value = 12; break;
            }
            return value;
        }


        public static void ProcessACMEntry(string entry, StreamWriter output)
        {
            try
            {
                
                var splitEntry = entry.Split(new char[] { ',', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                //var i = 0;
                if (splitEntry.Length != 9)
                {
                    return;
                }

                var i = 0;
                Match v;

                AbstractAcmResearch acm = null;

                if (splitEntry[i].Contains(Constants.Constants.Categories.Acm.ConferenceProceedings))
                {
                    acm = new ConferenceProceeding();
                }
                else if (splitEntry[i].Contains(Constants.Constants.Categories.Acm.Journals))
                {
                    acm = new Journal();
                }
                else if (splitEntry[i].Contains(Constants.Constants.Categories.Acm.Magazines))
                {
                    acm = new Magazine();
                }
                else if (splitEntry[i].Contains(Constants.Constants.Categories.Acm.Publications))
                {
                    acm = new Publication();
                }
                else if (splitEntry[i].Contains(Constants.Constants.Categories.Acm.SigNewsletters))
                {
                    acm = new SIGNewsletter();
                }
                else if (splitEntry[i].Contains(Constants.Constants.Categories.Acm.Transactions))
                {
                    acm = new Transaction();
                }
                else
                {
                    return;
                }

                // content type
                //acm.ContentType = splitEntry[++i];

                // get the title
                v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[++i]);
                acm.Title = v.Groups[1].Value;
                // title
                //acm.Title = splitEntry[++i];

                if(acm is ConferenceProceeding)
                {
                    i += 3;
                }
                else
                {

                    // abbr
                    v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[++i]);
                    acm.Abbr = v.Groups[1].Value;
                    // issn
                    v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[++i]);
                    acm.Issn = v.Groups[1].Value;
                    // eissn
                    v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[++i]);
                    acm.eIssn = v.Groups[1].Value;
                }

                v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[++i]);
                v = Constants.Constants.TagRegex.ACM.PublishingVolumeAndDate.Match(v.Groups[1].Value);
                acm.FirstPublished = v.Groups[1].Value;
                DateTime preciseDate;
                if (DateTime.TryParse(v.Groups[2].Value, out preciseDate))
                {
                    acm.FirstPublishedDateMonth = preciseDate.Month;
                    acm.FirstPublishedDateYear = preciseDate.Year;
                }
                else
                {
                    var date = v.Groups[2].Value.Split(' ');
                    acm.FirstPublishedDateMonth = MonthNameToNumber(date[0]);
                    acm.FirstPublishedDateYear = int.Parse(date[1]);
                }

                // latest published date month & year
                v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[i++]);
                v = Constants.Constants.TagRegex.ACM.PublishingVolumeAndDate.Match(v.Groups[1].Value);
                acm.LatestPublished = v.Groups[1].Value;

                if (DateTime.TryParse(v.Groups[2].Value, out preciseDate))
                {
                    acm.LatestPublishedDateMonth = preciseDate.Month;
                    acm.LatestPublishedDateYear = preciseDate.Year;
                }
                else
                {
                    var date = v.Groups[2].Value.Split(' ');
                    acm.LatestPublishedDateMonth = MonthNameToNumber(date[0]);
                    acm.LatestPublishedDateYear = int.Parse(date[1]);
                }

                v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[i++]);
                acm.ShortcutUrl = v.Groups[1].Value;

                v = Constants.Constants.TagRegex.ACM.StringValue.Match(splitEntry[i++]);
                acm.ArchiveUrl = v.Groups[1].Value;
                
                NHibernateHelper.Session.Save(acm);                                
            }
            catch(Exception ex)
            {

            }
            
        }
           
        public static void StoreACM()
        {         
            using (var stream = new StreamReader(Constants.Constants.LocalStoragePaths.ACM))
            using (var output = new StreamWriter(Constants.Constants.LocalStoragePaths.ACMTemporaryOutput))
            {
                var line = stream.ReadLine();
                line = stream.ReadLine();
                var count = 0;
                while ((line = stream.ReadLine()) != null)
                {
                    ProcessACMEntry(line, output);
                    ++count;
                    if (count % 100 == 0)
                    {
                        NHibernateHelper.Session.Flush();
                    }

                }
            }
            NHibernateHelper.Session.Flush();

            
        }

        public static void ProcessOnlineLibrary(Library library, WebClient client)
        {

            switch (library)
            {
                case Library.DBLP:
                    DownloadLibrary(Library.DBLP, client);
                    UnzipDBLP();
                    StoreDBLP();
                    return;
                case Library.ACM:
                    DownloadLibrary(Library.ACM, client);
                    StoreACM();
                    return;
                default:
                    return;
            }
            
        }
    }
}