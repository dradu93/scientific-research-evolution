﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.Models
{
    public class DblpPlot
    {
        [DisplayName("Articles")]
        public bool Articles { get; set; }

        [DisplayName("Inproceedings")]
        public bool Inproceedings { get; set; }

        [DisplayName("PhdThesis")]
        public bool PhdThesis { get; set; }

        [DisplayName("Www")]
        public bool Www { get; set; }

        //[DisplayName("Start month")]
        //public int StartMonth { get; set; }

        [DisplayName("Start Year")]
        public int StartYear { get; set; }

        //[DisplayName("End month")]
        //public int EndMonth { get; set; }

        [DisplayName("End year")]
        public int EndYear { get; set; }
    }
}