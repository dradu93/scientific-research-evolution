﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using System.Security.Cryptography;
using System.ComponentModel;
using System.Net;
using ScientificResearchEvolution.ResearchTypes.Dblp;
using ScientificResearchEvolution.ResearchTypes.Acm;

namespace ScientificResearchEvolution.Models
{
    /// <summary>
    /// Clasa cu metode utile pentru diverse operatii cu baza de date.
    /// </summary>
    public sealed class NHibernateHelper
    {
        public static void CreateDatabase()
        {
            
            var config = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(Constants.Constants.DatabaseConnectionString).ShowSql())
                .Mappings(m => m.FluentMappings
                .AddFromAssemblyOf<Article>()
                .AddFromAssemblyOf<Inproceeding>()
                .AddFromAssemblyOf<PhdThesis>()
                .AddFromAssemblyOf<WwwMap>()
                .AddFromAssemblyOf<ConferenceProceeding>()
                .AddFromAssemblyOf<Journal>()
                .AddFromAssemblyOf<Magazine>()
                .AddFromAssemblyOf<Publication>()
                .AddFromAssemblyOf<SIGNewsletter>()
                .AddFromAssemblyOf<Transaction>())
                .BuildConfiguration();

            var exporter = new SchemaExport(config);
            // drop the current database tables
            exporter.Execute(true, true, true);
            // read them
            exporter.Execute(true, true, false);
            
        }


        public static ISessionFactory BuildSessionFactory()
        {
            
            var sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(Constants.Constants.DatabaseConnectionString).ShowSql())
                .Mappings(m => m.FluentMappings
                .AddFromAssemblyOf<Article>()
                .AddFromAssemblyOf<Inproceeding>()
                .AddFromAssemblyOf<PhdThesis>()
                .AddFromAssemblyOf<WwwMap>()
                .AddFromAssemblyOf<ConferenceProceeding>()
                .AddFromAssemblyOf<Journal>()
                .AddFromAssemblyOf<Magazine>()
                .AddFromAssemblyOf<Publication>()
                .AddFromAssemblyOf<SIGNewsletter>()
                .AddFromAssemblyOf<Transaction>())
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                .BuildSessionFactory();
                
            return sessionFactory;
        }


        private static readonly ISessionFactory _sessionFactory = BuildSessionFactory();
        public static ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
        }

        private static readonly ISession _session = _sessionFactory.OpenSession();
        public static ISession Session
        {
            get { return _session; }
        }

        /// <summary>
        /// Opens a new NHibernate session.
        /// </summary>
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        public static ApplicationUser LoggerUser { get; set; }


        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        
    }
}