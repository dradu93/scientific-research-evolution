﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Acm
{
    /// <summary>
    /// Abstract class to describe a general ACM entry
    /// </summary>
    public abstract class AbstractAcmResearch
    {
        public virtual Guid Id { get; set; }
        //public virtual string ContentType { get; set; }
        public virtual string Title { get; set; }
        public virtual string Abbr { get; set; }
        public virtual string Issn { get; set; }
        public virtual string eIssn { get; set; }
        public virtual string FirstPublished { get; set; }
        public virtual int FirstPublishedDateMonth { get; set; }
        public virtual int FirstPublishedDateYear { get; set; }
        public virtual string LatestPublished { get; set; }
        public virtual int LatestPublishedDateMonth { get; set; }
        public virtual int LatestPublishedDateYear { get; set; }
        public virtual string ShortcutUrl { get; set; }
        public virtual string ArchiveUrl { get; set; }
    }
}