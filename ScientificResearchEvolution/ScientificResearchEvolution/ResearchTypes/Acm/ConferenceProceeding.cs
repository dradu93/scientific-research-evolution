﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Acm
{
    /// <summary>
    /// Used to describe "Conference Proceedings" 
    /// and "Conference Proceedings within Hosted Content"
    /// </summary>
    public class ConferenceProceeding : AbstractAcmResearch
    {
        public virtual bool WithHostedContent { get; set; }
    }

    public class ConferenceProceedingMap : ClassMap<ConferenceProceeding>{
        public ConferenceProceedingMap()
        {
            Table(Constants.Constants.TableNames.Acm.ConferenteProceedings);
            Id(t => t.Id).GeneratedBy.GuidComb();
            //Map(t => t.ContentType).Not.Nullable();
            Map(t => t.Title).Not.Nullable();
            Map(t => t.FirstPublished).Not.Nullable();
            Map(t => t.FirstPublishedDateMonth).Not.Nullable();
            Map(t => t.FirstPublishedDateYear).Not.Nullable();
            Map(t => t.LatestPublished).Not.Nullable();
            Map(t => t.LatestPublishedDateMonth).Not.Nullable();
            Map(t => t.LatestPublishedDateYear).Not.Nullable();
            Map(t => t.ShortcutUrl).Not.Nullable();
            Map(t => t.ArchiveUrl).Not.Nullable();
            Map(t => t.WithHostedContent).Not.Nullable();
        }
    }
}