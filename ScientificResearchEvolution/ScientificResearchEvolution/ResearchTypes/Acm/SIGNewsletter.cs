﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Acm
{
    /// <summary>
    /// Used to describe "SIG Newsletters"
    /// </summary>
    public class SIGNewsletter : AbstractAcmResearch
    {
    }

    public class SIGNewsletterMap : ClassMap<SIGNewsletter>
    {
        public SIGNewsletterMap()
        {
            Table(Constants.Constants.TableNames.Acm.SigNewsletter);
            Id(t => t.Id).GeneratedBy.GuidComb();
            //Map(t => t.ContentType).Not.Nullable();
            Map(t => t.Title).Not.Nullable();
            Map(t => t.Abbr).Not.Nullable();
            Map(t => t.Issn).Not.Nullable();
            Map(t => t.eIssn).Not.Nullable();
            Map(t => t.FirstPublished).Not.Nullable();
            Map(t => t.FirstPublishedDateMonth).Not.Nullable();
            Map(t => t.FirstPublishedDateYear).Not.Nullable();
            Map(t => t.LatestPublished).Not.Nullable();
            Map(t => t.LatestPublishedDateMonth).Not.Nullable();
            Map(t => t.LatestPublishedDateYear).Not.Nullable();
            Map(t => t.ShortcutUrl).Not.Nullable();
            Map(t => t.ArchiveUrl).Not.Nullable();
        }
    }
}