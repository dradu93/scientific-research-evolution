﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Dblp
{
    /// <summary>
    /// Used to describe an "Article"
    /// </summary>
    public class Article
    {
        public virtual Guid Id { get; set; }
        //public List<Author> Authors { get; set; }
        public virtual string Title { get; set; }
        public virtual string Pages { get; set; }
        public virtual int Year { get; set; }
        public virtual string Volume { get; set; }
        public virtual string Journal { get; set; }
        public virtual string Url { get; set; }
        public virtual string Ee { get; set; }
    }


    public class ArticleMap : ClassMap<Article> {
        public ArticleMap()
        {
            Table(Constants.Constants.TableNames.Dblp.Article);
            Id(t => t.Id).GeneratedBy.GuidComb();
            Map(t => t.Title).Length(2000);
            Map(t => t.Pages);
            Map(t => t.Year);
            Map(t => t.Volume);
            Map(t => t.Journal);
            Map(t => t.Url).Length(1000);
            Map(t => t.Ee).Length(1000);            
        }
    }
}