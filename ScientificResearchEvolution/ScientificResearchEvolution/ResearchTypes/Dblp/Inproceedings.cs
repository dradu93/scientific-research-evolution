﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Dblp
{
    /// <summary>
    /// Used to describe an "Improceeding"
    /// </summary>
    public class Inproceeding
    {
        public virtual Guid Id { get; set; }
        public virtual string Author { get; set; }
        public virtual string Title { get; set; }
        public virtual string Pages { get; set; }
        public virtual string Year { get; set; }
        public virtual string Booktitle { get; set; }
        public virtual string Ee { get; set; }
        public virtual string Crossref { get; set; }
        public virtual string Url { get; set; }
    }

    public class InproceedingsMap : ClassMap<Inproceeding>
    {
        public InproceedingsMap()
        {
            Table(Constants.Constants.TableNames.Dblp.Inproceedings);
            Id(t => t.Id).GeneratedBy.GuidComb();
            Map(t => t.Title).Length(1000);
            Map(t => t.Pages);
            Map(t => t.Year);
            Map(t => t.Booktitle).Length(1000);
            Map(t => t.Ee).Length(1000);
            Map(t => t.Crossref).Length(1000);
            Map(t => t.Url).Length(1000);
        }
    }
}