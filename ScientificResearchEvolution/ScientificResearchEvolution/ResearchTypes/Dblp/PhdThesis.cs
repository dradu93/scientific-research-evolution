﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Dblp
{
    /// <summary>
    /// Used to describe a "PhdThesis"
    /// </summary>
    public class PhdThesis
    {
        public virtual Guid Id { get; set; }
        //public virtual string Author { get; set; }
        public virtual string Title { get; set; }
        public virtual int Year { get; set; }
        public virtual string School { get; set; }
        public virtual string Ee { get; set; }
        public virtual string Note { get; set; }
    }

    public class PhdThesisMap : ClassMap<PhdThesis>
    {
        public PhdThesisMap()
        {
            Table(Constants.Constants.TableNames.Dblp.PhdThesis);
            Id(t => t.Id).GeneratedBy.GuidComb();
            //Map(t => t.Author).Length(100);
            Map(t => t.Title).Length(1000);
            Map(t => t.Year);
            Map(t => t.School).Length(500);
            Map(t => t.Ee).Length(500);
            Map(t => t.Note).Length(500);
        }
    }
}