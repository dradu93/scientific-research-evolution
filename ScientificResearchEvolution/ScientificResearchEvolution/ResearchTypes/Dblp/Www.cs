﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScientificResearchEvolution.ResearchTypes.Dblp
{
    /// <summary>
    /// Used to describe a "Www"
    /// </summary>
    public class Www
    {
        public virtual Guid Id { get; set; }
        public virtual string Author { get; set; }
        public virtual string Title { get; set; }
    }

    public class WwwMap : ClassMap<Www>
    {
        public WwwMap()
        {
            Table(Constants.Constants.TableNames.Dblp.Www);
            Id(t => t.Id).GeneratedBy.GuidComb();
            Map(t => t.Title).Length(500);
        }
        
    }
}