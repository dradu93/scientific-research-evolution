﻿using Microsoft.Owin;
using Owin;
using ScientificResearchEvolution.Models;

[assembly: OwinStartupAttribute(typeof(ScientificResearchEvolution.Startup))]
namespace ScientificResearchEvolution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["RecreateDatabase"].ToString().Equals("true"))
            {
                NHibernateHelper.CreateDatabase();
            }
            ConfigureAuth(app);
        }
    }
}
